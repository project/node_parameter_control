<?php
/**
 * @file
 * Implements fine grained control of node edit parameters.
 */
// Set variables
$_node_parameter_control_contenttype = '';
function node_parameter_control_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/node_parameter_control',
      'title' => t('Node Parameter Control'),
      'description' => t('Enable/disable the display of certain parameters on the node edit pages'),
      'callback' => 'node_parameter_control',
      'access' => user_access('Administer Node Parameters')
    );
    $items[] = array(
      'path' => 'admin/settings/node_parameter_control/settings',
      'title' => t('Node Parameter Control Settings'),
      'description' => t('Enable/disable the display of certain parameters on the node edit pages'),
      'callback' => 'node_parameter_control_settings',
      'access' => TRUE,
      'type' => MENU_CALLBACK
    );
  }
  else {
    $path = drupal_get_path('module', 'node_parameter_control');
    drupal_add_css($path .'/node_parameter_control.css', 'module', 'all', FALSE);
  }
  return $items;
}
function node_parameter_control_form_alter($form_id, &$form) {
  /*  On the main content admin page, the publishing opton is disabled if the current user has publishing options
      turned off for ANY CONTENT TYPE. This is treated globally here because this this page is so powerful. */
  if ($form_id == 'node_admin_nodes') {
    if (_publishPermission() == false) {
      $form['options'] = array(
        '#type' => 'fieldset',
        '#title' => t('Update options'),
        '#prefix' => '<div class="container-inline">',
        '#suffix' => '</div>',
        'operation' => array (
            '#type' => 'select',
            '#options' => array (
                    'promote' => t('Promote to front page'),
                    'demote' => t('Demote from front page'),
                    'sticky' => t('Make sticky'),
                    'unsticky' => t('Remove stickiness'),
                    'delete' => t('Delete')
            ),
            '#default_value' => 'approve'
        ),
        'submit' => array (
            '#type' => 'submit',
            '#value' => t('Update')
        )
      );
    }
  }
}
/*  Called from the hook_form_alter if we are on the main content admin page.
    Used to disable the publish option. Unlike the other individual node edit pages
    This is a global change, it removes the publishing option for all content types,
    even if the user's publishing option is disabled for even just one content type.
    This is necessary because the main content admin page is so powerful. */
function _publishPermission() {
  global $user;
  // Pull the roles of the current user
  $current_roles = $user->roles;
  if ($user->uid == 1) {
    $current_roles[] = 'root';
  }
  // Pull the rules for ALL content types
  $allNode_rules_serialized = db_fetch_array(db_query("SELECT data FROM {node_parameter_control}"));
    if (count($allNode_rules_serialized)) {
      foreach ($allNode_rules_serialized as $node_rules_serialized) {
        $node_rules_US = unserialize($node_rules_serialized);
        if (count($node_rules_US)) {
          foreach ($node_rules_US as $rsK => $rule_set) {
          // Check to make sure the ruleset is matched to a current role
          // The ruleset key is the name of the role it should be applied to
          // But we have to swap underscores with spaces to match the way roles are stored by Drupal.
          // If there's a match, process the form exclusions
          if (in_array(str_replace('_', ' ', $rsK),$current_roles)) {
            foreach ($rule_set as $k => $v) {
              // If there's any instance of a "no publishing" flag, disable the publishing option and drop out of the loop.
              if ($k == 'options' && $v) {
                return false;
              }
            }
          }
        }
      }
    }
  }
  return true;
}
function node_parameter_control_perm() {
  return array('Administer Node Parameters');
}
// The admin page
function node_parameter_control() {
  // Gather all content types and print them out in a list of links.
  $result = db_query("SELECT type FROM {node_type}");
  $output = '<p>'. t('The granular node admin module allows finegrain control over the the display of base parameters in the node edit pages. It\'s recommended you first try to accomplish your requirements with the ');
  $output .= l('access control settings', 'admin/user/access');
  $output .= ' before resorting to this module</p>';
  $output .= '<h3>'. t('Content Types') .'</h3>';
  while ($data = db_fetch_object($result)) {
    $output .= '<p>'. l($data->type, "admin/settings/node_parameter_control/settings/" . $data->type) .'</p>';
  }
  return $output;
}
function node_parameter_control_settings($type) {
  $output = '<h2>'. t(arg(4)) .'</h2>';
  $output .= '<p id="backtoNodeParamControlAdmin">'. l('Back to content type list', 'admin/settings/node_parameter_control/') .'</p>';
  $output .= '<p>'. t('Selecting a checkbox DISABLES the form parameter for that role. For users with multiple roles, the settings here are cumulative. All roles inherit the settings for "Authenticated User".<br /><br />WARNING: selecting "Publishing Options" for even just one content type will disable the publishing options for all content types in the ') . l('main content admin page', 'admin/content/node') . t('. But the user will still be able to publish their allowed content types on the individual node edit pages.') .'</p>';
  $output .= drupal_get_form('node_parameter_control_form', $type);
  return $output;
}
function node_parameter_control_form($type) {
  global $user;
  $all_roles = user_roles(TRUE);
  if ($user->uid == 1) {
    $all_roles[] = 'root';
  }
  $node_rules_serialized = db_result(db_query("SELECT data FROM {node_parameter_control} WHERE type = '%s'", arg(4)));
  $node_rules_US = unserialize($node_rules_serialized);
  $form['#redirect'] = 'admin/settings/node_parameter_control';
  $form['settings'] = array(
      '#type' => 'fieldset',
      '#tree' => TRUE
  );
  //LABELS
  $form['settings']['listStart'] = array(
    '#value' => '<table><tr class="labels">'
  );
  $form['settings']['label_role'] = array(
    '#prefix' => '<td class="col_role">',
    '#value' => t('<br />Role'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_body'] = array(
    '#prefix' => '<td class="col_body">',
    '#value' => t('<br />Body'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_inputFormat'] = array(
    '#prefix' => '<td class="col_inputformat">',
    '#value' => t('Input<br />Format'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_logMessage'] = array(
    '#prefix' => '<td class="col_log">',
    '#value' => t('Log<br />Message'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_authoringInfo'] = array(
    '#prefix' => '<td class="col_author">',
    '#value' => t('Authoring<br />Infomation'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_publisingInfo'] = array(
    '#prefix' => '<td class="col_pub">',
    '#value' => t('Publishing<br />Options'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_menuSettings'] = array(
    '#prefix' => '<td class="col_menu">',
    '#value' => t('Menu<br />Settings'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_commentSettings'] = array(
    '#prefix' => '<td class="col_comment">',
    '#value' => t('Comment<br />Settings'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_pathSettings'] = array(
    '#prefix' => '<td class="col_path">',
    '#value' => t('URL Path<br />Settings'),
    '#suffix' => '</td>'
  );
  $form['settings']['label_attachments'] = array(
    '#prefix' => '<td class="col_file">',
    '#value' => t('File<br />Attachments'),
    '#suffix' => '</td>'
  );
  $form['settings']['labelsEnd'] = array(
    '#value' => '</tr>'
  );
  // RULESETS by ROLE
  foreach ($all_roles as $role) {
    $role = str_replace(' ', '_', $role);
    $form['settings'][$role][$role .'-listStart'] = array(
      '#value' => '<tr class="settings">'
    );
    $form['settings'][$role][$role .'_role'] = array(
      '#prefix' => '<td class="col_role">',
      '#value' => t(str_replace('_', '<br />', $role)),
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['body_filter'] = array(
      '#prefix' => '<td class="col_body">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['body_filter'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['body_filter_format'] = array(
      '#prefix' => '<td class="col_inputformat">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['body_filter_format'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['log'] = array(
      '#prefix' => '<td class="col_log">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['log'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['author'] = array(
      '#prefix' => '<td class="col_author">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['author'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['options'] = array(
      '#prefix' => '<td class="col_pubOptions">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['options'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['menu'] = array(
      '#prefix' => '<td class="col_menu">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['menu'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['comment_settings'] = array(
      '#prefix' => '<td class="col_comment">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['comment_settings'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['path'] = array(
      '#prefix' => '<td class="col_path">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['path'],
      '#suffix' => '</td>'
    );
    $form['settings'][$role]['attachments'] = array(
      '#prefix' => '<td class="col_file">',
      '#type' => 'checkbox',
      '#default_value' => $node_rules_US[$role]['attachments'],
      '#suffix' => '</td></tr>'
    );
  }
  $form['settings'][$role][$role .'-listEnd'] = array(
    '#value' => '</table>'
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );
  return $form;
}
function node_parameter_control_form_submit($form_id, $form_values) {
  $output = array();
  // First remove some form values we don't need. Then prepare the remaining form data for saving in the DB
  unset($form_values['op'], $form_values['submit'], $form_values['form_token'], $form_values['form_id']);
  foreach ($form_values['settings'] as $k => $v) {
    $output[$k] = array();
    foreach ($form_values['settings'][$k] as $k1 => $v1) {
      if ($v1 == '1') {
        $output[$k][$k1] = $v1;
      }
    }
  }
  _save_granular_settings(serialize($output), arg(4));
  drupal_set_message('The settings have been saved.');
}
function _save_granular_settings($serialized_data, $ctype) {
  // We only one one record per content type
  $record_exists = db_result(db_query("SELECT COUNT(*) FROM {node_parameter_control} WHERE type = '%s'", $ctype));
  if ($record_exists) {
    db_query("UPDATE {node_parameter_control} SET data = '%s' WHERE type = '%s'", $serialized_data, $ctype);
  }
  else {
    db_query("INSERT INTO {node_parameter_control} (type, data) VALUES ('%s','%s')", $ctype, $serialized_data);
  }
}
// If a content type is deleted, delete it's matching records here.
function node_parameter_control_node_type($op, $info) {
  switch ($op) {
    case 'delete':
      db_query("DELETE FROM {node_parameter_control} WHERE type = '%s'", $info->type);
      break;
  }
}
// We determine the node type here for use below
function node_parameter_control_nodeapi(&$node, $op) {
  global $_node_parameter_control_contenttype;
  switch ($op) {
    case 'prepare':
      $_node_parameter_control_contenttype = $node->type;
  }
}
// Hide the selected parameters in the the node "add" and "edit" pages.
function phptemplate_node_form($form) {
  global $user, $_node_parameter_control_contenttype;
  // Pull the roles of the current user
  $current_roles = $user->roles;
  if ($user->uid == 1) {
    $current_roles[] = 'root';
  }
  // Pull the rules for the content type
  $node_rules_serialized = db_result(db_query("SELECT data FROM {node_parameter_control} WHERE type = '%s'", $_node_parameter_control_contenttype));
  $node_rules_US = unserialize($node_rules_serialized);
  // Apply the rules for the content type
  if ($node_rules_US != '') {
    foreach ($node_rules_US as $rsK => $rule_set) {
      // Check to make sure the ruleset is matched to a current role
      // The ruleset key is the name of the role it should be applied to
      // But we have to swap underscores with spaces to match the way roles are stored by Drupal.
      // If there's a match, process the form exclusions
      if (in_array(str_replace('_', ' ', $rsK),$current_roles)) {
        foreach ($rule_set as $k => $v) {
          // The input format array key format is different from the others. It needs special handling
          if ($k == 'body_filter_format') {
            $form['body_filter']['format']['#access'] = FALSE;
          }
          $form[$k]['#access'] = FALSE;
        }
      }
    }
  }
  return drupal_render($form);
}